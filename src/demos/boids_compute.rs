use crate::bevy_engine_layer::*;
use crate::core::stage_configuration_plugin::{StageExecutionLabel, SystemExecutionLabel};
use crate::renderer::render_systems::{RenderState, SurfaceFrameCarrier};
use rand::distributions::Uniform;
use rand::prelude::Distribution;
use rand::SeedableRng;
use std::borrow::Cow;
use std::mem;
use wgpu::util::DeviceExt;

#[derive(Default)]
pub struct BoidsComputePlugin;

pub struct BoidsComputeData {
    particle_bind_groups: Vec<wgpu::BindGroup>,
    particle_buffers: Vec<wgpu::Buffer>,
    vertices_buffer: wgpu::Buffer,
    compute_pipeline: wgpu::ComputePipeline,
    render_pipeline: wgpu::RenderPipeline,
    work_group_count: u32,
    frame_num: usize,
}

impl Plugin for BoidsComputePlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(init_boids_sys).add_system_to_stage(
            StageExecutionLabel::RenderPrimary,
            compute_render_boids_sys.after(SystemExecutionLabel::MainRender),
        );

        /*app.add_startup_system_to_stage(StartupStage::Startup, initialize_renderer_sys.exclusive_system().label(SystemExecutionLabel::MainRender))
           .add_startup_system_to_stage(StartupStage::Startup, init_test_sys)
           .add_system_to_stage(StageExecutionLabel::RenderPrimary, render_sys)
           .add_system_to_stage(StageExecutionLabel::PostRender, post_render_sys)
           .add_system_to_stage(CoreStage::PostUpdate, resize_window_event_handler)
           .add_event::<WindowResizedEvent>()
        ;*/
    }
}

//TODO : Compute shader systems should be split into compute/draw independence, so we can start compute early in the frame
// and let the GPU get to work while we're iterating update systems on CPU
fn compute_render_boids_sys(
    mut boids_data: ResMut<BoidsComputeData>,
    mut render_state: ResMut<RenderState>,
    mut frame_carrier: ResMut<SurfaceFrameCarrier>,
) {
    // We need to fish out the frame previously acquired from render_sys and render on top of it rather than acquiring a new one
    let frame = frame_carrier
        .frame
        .take()
        .expect("swapchain frame not found in imgui_render_sys");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    let color_attachments = [wgpu::RenderPassColorAttachment {
        view: &view,
        resolve_target: None,
        ops: wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
            store: true,
        },
    }];
    let render_pass_descriptor = wgpu::RenderPassDescriptor {
        label: None,
        color_attachments: &color_attachments,
        depth_stencil_attachment: None,
    };

    // get command encoder
    let mut command_encoder = render_state
        .device
        .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

    command_encoder.push_debug_group("compute boid movement");
    {
        // compute pass
        let mut cpass =
            command_encoder.begin_compute_pass(&wgpu::ComputePassDescriptor { label: None });
        cpass.set_pipeline(&boids_data.compute_pipeline);
        cpass.set_bind_group(
            0,
            &boids_data.particle_bind_groups[boids_data.frame_num % 2],
            &[],
        );
        cpass.dispatch(boids_data.work_group_count, 1, 1);
    }
    command_encoder.pop_debug_group();

    command_encoder.push_debug_group("render boids");
    {
        // render pass
        let mut rpass = command_encoder.begin_render_pass(&render_pass_descriptor);
        rpass.set_pipeline(&boids_data.render_pipeline);
        // render dst particles
        rpass.set_vertex_buffer(
            0,
            boids_data.particle_buffers[(boids_data.frame_num + 1) % 2].slice(..),
        );
        // the three instance-local vertices
        rpass.set_vertex_buffer(1, boids_data.vertices_buffer.slice(..));
        rpass.draw(0..3, 0..NUM_PARTICLES);
    }
    command_encoder.pop_debug_group();

    // update frame count
    boids_data.frame_num += 1;

    // done
    render_state.queue.submit(Some(command_encoder.finish()));

    frame_carrier.frame = Some(frame);
}

fn init_boids_sys(mut commands: Commands, mut render_state: ResMut<RenderState>) {
    //let mut device = &mut render_state.device;

    let compute_src = std::fs::read_to_string("assets/shaders/boids_compute.wsgl")
        .expect("Couldn't read shader.wsgl");
    let draw_src = std::fs::read_to_string("assets/shaders/boids_draw.wsgl")
        .expect("Couldn't read shader.wsgl");

    let compute_shader = render_state
        .device
        .create_shader_module(&wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(&*compute_src)),
        });
    let draw_shader = render_state
        .device
        .create_shader_module(&wgpu::ShaderModuleDescriptor {
            label: None,
            source: wgpu::ShaderSource::Wgsl(Cow::Borrowed(&*draw_src)),
        });

    let sim_param_data = [
        0.04f32, // deltaT
        0.1,     // rule1Distance
        0.025,   // rule2Distance
        0.025,   // rule3Distance
        0.02,    // rule1Scale
        0.05,    // rule2Scale
        0.005,   // rule3Scale
    ]
    .to_vec();

    let sim_param_buffer =
        render_state
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Simulation Parameter Buffer"),
                contents: bytemuck::cast_slice(&sim_param_data),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

    // create compute bind layout group and compute pipeline layout

    let compute_bind_group_layout =
        render_state
            .device
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Uniform,
                            has_dynamic_offset: false,
                            min_binding_size: wgpu::BufferSize::new(
                                (sim_param_data.len() * mem::size_of::<f32>()) as _,
                            ),
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: true },
                            has_dynamic_offset: false,
                            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 16) as _),
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 2,
                        visibility: wgpu::ShaderStages::COMPUTE,
                        ty: wgpu::BindingType::Buffer {
                            ty: wgpu::BufferBindingType::Storage { read_only: false },
                            has_dynamic_offset: false,
                            min_binding_size: wgpu::BufferSize::new((NUM_PARTICLES * 16) as _),
                        },
                        count: None,
                    },
                ],
                label: None,
            });
    let compute_pipeline_layout =
        render_state
            .device
            .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("compute"),
                bind_group_layouts: &[&compute_bind_group_layout],
                push_constant_ranges: &[],
            });

    // create render pipeline with empty bind group layout

    let render_pipeline_layout =
        render_state
            .device
            .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("render"),
                bind_group_layouts: &[],
                push_constant_ranges: &[],
            });

    let render_pipeline =
        render_state
            .device
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: None,
                layout: Some(&render_pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &draw_shader,
                    entry_point: "main",
                    buffers: &[
                        wgpu::VertexBufferLayout {
                            array_stride: 4 * 4,
                            step_mode: wgpu::VertexStepMode::Instance,
                            attributes: &wgpu::vertex_attr_array![0 => Float32x2, 1 => Float32x2],
                        },
                        wgpu::VertexBufferLayout {
                            array_stride: 2 * 4,
                            step_mode: wgpu::VertexStepMode::Vertex,
                            attributes: &wgpu::vertex_attr_array![2 => Float32x2],
                        },
                    ],
                },
                fragment: Some(wgpu::FragmentState {
                    module: &draw_shader,
                    entry_point: "main",
                    //targets: &[config.format.into()],
                    targets: &[render_state.surface_desc.format.into()],
                }),
                primitive: wgpu::PrimitiveState::default(),
                depth_stencil: None,
                multisample: wgpu::MultisampleState::default(),
            });

    // create compute pipeline

    let compute_pipeline =
        render_state
            .device
            .create_compute_pipeline(&wgpu::ComputePipelineDescriptor {
                label: Some("Compute pipeline"),
                layout: Some(&compute_pipeline_layout),
                module: &compute_shader,
                entry_point: "main",
            });

    // buffer for the three 2d triangle vertices of each instance

    let vertex_buffer_data = [-0.01f32, -0.02, 0.01, -0.02, 0.00, 0.02];
    let vertices_buffer =
        render_state
            .device
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::bytes_of(&vertex_buffer_data),
                usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
            });

    // buffer for all particles data of type [(posx,posy,velx,vely),...]

    let mut initial_particle_data = vec![0.0f32; (4 * NUM_PARTICLES) as usize];
    let mut rng = rand::rngs::StdRng::seed_from_u64(42);
    let unif = Uniform::new_inclusive(-1.0, 1.0);
    for particle_instance_chunk in initial_particle_data.chunks_mut(4) {
        particle_instance_chunk[0] = unif.sample(&mut rng); // posx
        particle_instance_chunk[1] = unif.sample(&mut rng); // posy
        particle_instance_chunk[2] = unif.sample(&mut rng) * 0.1; // velx
        particle_instance_chunk[3] = unif.sample(&mut rng) * 0.1; // vely
    }

    // creates two buffers of particle data each of size NUM_PARTICLES
    // the two buffers alternate as dst and src for each frame

    let mut particle_buffers = Vec::<wgpu::Buffer>::new();
    let mut particle_bind_groups = Vec::<wgpu::BindGroup>::new();
    for i in 0..2 {
        particle_buffers.push(render_state.device.create_buffer_init(
            &wgpu::util::BufferInitDescriptor {
                label: Some(&format!("Particle Buffer {}", i)),
                contents: bytemuck::cast_slice(&initial_particle_data),
                usage: wgpu::BufferUsages::VERTEX
                    | wgpu::BufferUsages::STORAGE
                    | wgpu::BufferUsages::COPY_DST,
            },
        ));
    }

    // create two bind groups, one for each buffer as the src
    // where the alternate buffer is used as the dst

    for i in 0..2 {
        particle_bind_groups.push(render_state.device.create_bind_group(
            &wgpu::BindGroupDescriptor {
                layout: &compute_bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: sim_param_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: particle_buffers[i].as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: particle_buffers[(i + 1) % 2].as_entire_binding(), // bind to opposite buffer
                    },
                ],
                label: None,
            },
        ));
    }

    // calculates number of work groups from PARTICLES_PER_GROUP constant
    let work_group_count = ((NUM_PARTICLES as f32) / (PARTICLES_PER_GROUP as f32)).ceil() as u32;

    //  Shove init'd data into resource
    commands.insert_resource(BoidsComputeData {
        particle_bind_groups,
        particle_buffers,
        vertices_buffer,
        compute_pipeline,
        render_pipeline,
        work_group_count,
        frame_num: 0,
    });
}

// number of boid particles to simulate

const NUM_PARTICLES: u32 = 1500;

// number of single-particle calculations (invocations) in each gpu work group

const PARTICLES_PER_GROUP: u32 = 64;
