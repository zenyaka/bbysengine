use crate::bevy_engine_layer::{App, Plugin, Query, ResMut};
use crate::core::stage_configuration_plugin::{StageExecutionLabel, SystemExecutionLabel};
use crate::renderer::render_components::RenderComp;
use crate::renderer::render_systems::{RenderState, SurfaceFrameCarrier};
use crate::renderer::texture::TextureComp;
use crate::renderer::types::buffers::{IndexBuffer, VertexBuffer};
use crate::renderer::types::pipelines::{Pipeline, PipelineType};
use crate::renderer::types::Vertex;
use bevy_ecs::schedule::ParallelSystemDescriptorCoercion;
use bevy_ecs::system::Commands;
use image::GenericImageView;
use std::borrow::Cow;
use std::sync::Arc;
use wgpu::util::DeviceExt;
use wgpu::{BindGroup, BindGroupLayout, Device, TextureDimension};

const VERTICES: &[Vertex] = &[
    Vertex {
        position: [-0.0868241, 0.49240386, 0.0],
        tex_coords: [0.4131759, 0.00759614],
    }, // A
    Vertex {
        position: [-0.49513406, 0.06958647, 0.0],
        tex_coords: [0.0048659444, 0.43041354],
    }, // B
    Vertex {
        position: [-0.21918549, -0.44939706, 0.0],
        tex_coords: [0.28081453, 0.949397],
    }, // C
    Vertex {
        position: [0.35966998, -0.3473291, 0.0],
        tex_coords: [0.85967, 0.84732914],
    }, // D
    Vertex {
        position: [0.44147372, 0.2347359, 0.0],
        tex_coords: [0.9414737, 0.2652641],
    }, // E
];

const INDICES: &[u16] = &[
    0, 1, 4, 1, 2, 4, 2, 3, 4,
    0, //add 2 bytes of padding bc wgpu requires buffers aligned to 4 bytes
];

#[derive(Default)]
pub struct TextureRenderPlugin;

impl Plugin for TextureRenderPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(init_texture_sys)
            .add_system_to_stage(
                StageExecutionLabel::RenderPrimary,
                render_textured_sys.after(SystemExecutionLabel::MainRender),
            );
    }
}

fn init_texture_sys(mut commands: Commands, mut render_state: ResMut<RenderState>) {
    let vertex_buffer = render_state
        .device
        .create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Texture Vertex Buffer"),
            contents: bytemuck::cast_slice(VERTICES),
            usage: wgpu::BufferUsages::VERTEX,
        });

    let index_buffer = render_state
        .device
        .create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Texture Index Buffer"),
            contents: bytemuck::cast_slice(INDICES),
            usage: wgpu::BufferUsages::INDEX,
        });

    let bind_group_layout =
        render_state
            .device
            .create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler {
                            // This is only for TextureSampleType::Depth
                            comparison: false,
                            // This should be true if the sample_type of the texture is:
                            //     TextureSampleType::Float { filterable: true }
                            // Otherwise you'll get an error.
                            filtering: true,
                        },
                        count: None,
                    },
                ],
                label: Some("texture_bind_group_layout"),
            });

    let diffuse_bytes = std::fs::read("assets/textures/happy-tree.png").expect("Texture not found");

    let texture_comp: TextureComp = TextureComp::from_bytes(
        &render_state.device,
        &render_state.queue,
        &bind_group_layout,
        &diffuse_bytes,
        "happy-tree.png",
    )
    .unwrap();

    let render_pipeline_layout =
        render_state
            .device
            .create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[&bind_group_layout],
                push_constant_ranges: &[],
            });

    //need to compile shader here
    let shader = compile_shader(&render_state.device);

    let render_pipeline =
        render_state
            .device
            .create_render_pipeline(&wgpu::RenderPipelineDescriptor {
                label: Some("Render Pipeline"),
                layout: Some(&render_pipeline_layout),
                vertex: wgpu::VertexState {
                    module: &shader,
                    entry_point: "vs_main", // shader main fn to call, can just always be main
                    buffers: &[Vertex::desc()], // type of vertices to pass to vertex shader
                },
                fragment: Some(wgpu::FragmentState {
                    // This is optional, but required to store color data to swap chain
                    module: &shader,
                    entry_point: "fs_main",
                    targets: &[render_state.surface_format.into()],
                }),
                primitive: wgpu::PrimitiveState {
                    topology: wgpu::PrimitiveTopology::TriangleList,
                    strip_index_format: None,
                    front_face: wgpu::FrontFace::Ccw, // 5
                    cull_mode: Some(wgpu::Face::Back),
                    // Requires Features::DEPTH_CLAMPING
                    clamp_depth: false,
                    // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
                    polygon_mode: wgpu::PolygonMode::Fill,
                    // Requires Features::CONSERVATIVE_RASTERIZATION
                    conservative: false,
                },
                depth_stencil: None, // Todo: Set up depth stencil
                multisample: wgpu::MultisampleState {
                    count: 1,                         // just disable it for now
                    mask: !0,                         // Use every sample
                    alpha_to_coverage_enabled: false, // This is for AA l8r
                },
            });

    let pipeline = Pipeline {
        pipeline: render_pipeline,
        bind_group_layout: Some(bind_group_layout),
    };

    render_state
        .render_pipeline_cache
        .insert(PipelineType::Textured, pipeline);

    log::warn!("Spawning render/texture comp");

    commands
        .spawn()
        .insert(RenderComp {
            vertex_buffer: VertexBuffer(vertex_buffer),
            num_vertices: VERTICES.len() as u32,
            index_buffer: Some(IndexBuffer(index_buffer)),
            num_indices: INDICES.len() as u32,
        })
        .insert(texture_comp);
}

fn compile_shader(device: &Arc<Device>) -> wgpu::ShaderModule {
    let shader_src = std::fs::read_to_string("assets/shaders/textured.wsgl")
        .expect("Couldn't read textured.wsgl");
    let shader = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: Some("TexturedShader"),
        source: wgpu::ShaderSource::Wgsl(Cow::from(shader_src)).into(),
    });

    shader
}

fn render_textured_sys(
    mut render_state: ResMut<RenderState>,
    mut frame_carrier: ResMut<SurfaceFrameCarrier>,
    query: Query<(&RenderComp, &TextureComp)>,
) {
    let frame = frame_carrier
        .frame
        .take()
        .expect("swapchain frame not found in imgui_render_sys");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    for (rendercmp, texturecmp) in query.iter() {
        let mut encoder =
            render_state
                .device
                .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                    label: Some("Render Encoder"),
                });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    //ops: wgpu::Operations { load: wgpu::LoadOp::Clear(wgpu::Color { r: 0.1, g: 0.2, b: 0.3, a: 1.0 }), store: true },
                    ops: wgpu::Operations {
                        // DO NOT CLEAR THE SCREEN HERE
                        load: wgpu::LoadOp::Load,
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            let texture_pipeline = render_state
                .render_pipeline_cache
                .get(&PipelineType::Textured)
                .expect("Pipeline cache missing textured");

            render_pass.set_pipeline(&texture_pipeline.pipeline);
            render_pass.set_bind_group(0, &texturecmp.bind_group, &[]);
            render_pass.set_vertex_buffer(0, rendercmp.vertex_buffer.0.slice(..));
            render_pass.set_index_buffer(
                rendercmp.index_buffer.as_ref().unwrap().0.slice(..),
                wgpu::IndexFormat::Uint16,
            );
            render_pass.draw_indexed(0..rendercmp.num_indices, 0, 0..1);
        }

        // Render main
        render_state.queue.submit(std::iter::once(encoder.finish()));
    }

    frame_carrier.frame = Some(frame);
}
