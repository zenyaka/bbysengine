//These are just for ease of use because they're all required
//for the ecs loop to work and this allows us to just
//pub mod bevy_engine_layer once for all of it
pub use bevy_app::prelude::*;
pub use bevy_core::prelude::*;
pub use bevy_ecs::prelude::*;

pub use bevy_input::keyboard::KeyCode;

//This sets up the bare basics required for the ECS loop to function
//And should always be added to the main App
pub use bevy_app::{AppExit, PluginGroup, PluginGroupBuilder};

//Convenience to avoid spewing them everywhere
pub use log::{debug, error, info, trace, warn};

pub struct BevyInternalPlugins;

pub type SystemResult = anyhow::Result<()>;

impl PluginGroup for BevyInternalPlugins {
    fn build(&mut self, group: &mut PluginGroupBuilder) {
        group.add(bevy_core::CorePlugin::default());
        group.add(bevy_input::InputPlugin::default());
    }
}
