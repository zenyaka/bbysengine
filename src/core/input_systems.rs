use crate::bevy_engine_layer::*;
use crate::core::ecs_events::CursorPositionChangedEvent;
use crate::core::errors::system_error_handler;
use bevy_input::keyboard::KeyboardInput;
use bevy_input::mouse::{MouseButtonInput, MouseMotion};
use bevy_input::Input;

#[derive(Default)]
pub struct InputHandlingPlugin;

impl Plugin for InputHandlingPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<CursorPositionChangedEvent>()
            .add_system_to_stage(
                CoreStage::Update,
                handle_keyboard_event_sys.chain(system_error_handler),
            )
            .add_system_to_stage(CoreStage::Update, handle_mouse_event_sys)
            .add_system_to_stage(CoreStage::Update, test_keyboard_polling_sys);
    }
}

pub fn handle_keyboard_event_sys(
    mut keyboard_input_events: EventReader<KeyboardInput>,
) -> SystemResult {
    for event in keyboard_input_events.iter() {
        info!("{:?}", event);
    }
    Ok(())
}

pub fn test_keyboard_polling_sys(keyboard_input: Res<Input<KeyCode>>) {
    if keyboard_input.pressed(KeyCode::A) {
        info!("'A' currently pressed");
    }

    if keyboard_input.just_pressed(KeyCode::A) {
        info!("'A' just pressed");
    }

    if keyboard_input.just_released(KeyCode::A) {
        info!("'A' just released");
    }
}

pub fn handle_mouse_event_sys(
    mut mouse_events: EventReader<MouseButtonInput>,
    mut mouse_delta_events: EventReader<MouseMotion>,
    mut mouse_position_events: EventReader<CursorPositionChangedEvent>,
) {
    for event in mouse_events.iter() {
        info!("{:?}", event);
    }
    for event in mouse_delta_events.iter() {
        debug!("{:?}", event);
    }
    for event in mouse_position_events.iter() {
        debug!("{:?}", event);
    }
}
