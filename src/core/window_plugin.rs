use crate::bevy_engine_layer::*;
use crate::core::converters::{
    convert_element_state, convert_keyboard_input, convert_mouse_button, convert_mouse_position,
};
use crate::core::ecs_events::{CursorPositionChangedEvent, WindowResizedEvent};
use crate::renderer::imgui_systems::{ImguiCtxState, ImguiPlatformState};
use bevy_app::Events;
use bevy_input::keyboard::KeyboardInput;
use bevy_input::mouse::{MouseButtonInput, MouseMotion};
use bevy_math::Vec2;
use log::{info, trace};
use winit::event::{Event, WindowEvent};
use winit::event_loop::ControlFlow;
use winit::window::{Window as WinitWindow, WindowBuilder};

pub struct Window {
    pub window: WinitWindow,
}

impl Window {
    pub fn new(event_loop: &mut winit::event_loop::EventLoop<()>) -> Self {
        let winit_window_builder = { WindowBuilder::new().with_title("BbysEngine") };

        Window {
            window: winit_window_builder.build(&event_loop).unwrap(),
        }
    }
}

#[derive(Default)]
pub struct WindowPlugin;

impl Plugin for WindowPlugin {
    fn build(&self, app: &mut App) {
        app.set_runner(winit_event_runner);
        // add the conversion systems
    }
}

fn winit_event_runner(mut app: App) {
    trace!("In event runner");
    let mut event_loop = winit::event_loop::EventLoop::new();
    {
        let window = Window::new(&mut event_loop);
        app.world.insert_resource(window);
    }

    let mut old_time = std::time::Instant::now();
    const MSPT: std::time::Duration = std::time::Duration::from_millis(20);

    //stops rendering if screen is minimized
    let mut active = true;
    let mut previous_cursor_pos = Vec2::new(0f32, 0f32);

    event_loop.run(move |main_event, _event_loop, control_flow| {
        let world = app.world.cell();

        let platform_opt = world.get_non_send_mut::<ImguiPlatformState>();
        let window = world.get_resource::<Window>();
        let imgui_ctx = world.get_non_send_mut::<ImguiCtxState>();
        let mut should_send_key = true;
        let mut should_send_mouse = true;

        match (platform_opt, window, imgui_ctx) {
            (Some(mut platform_opt), Some(window), Some(mut imgui)) => {
                platform_opt.platform.handle_event(
                    imgui.context.io_mut(),
                    &window.window,
                    &main_event,
                );
                //Note these are inverted, because imgui reports whether IT wants to keep input or not
                should_send_key = !imgui.context.io().want_capture_keyboard;
                should_send_mouse = !imgui.context.io().want_capture_mouse;
            }
            (None, _, _) => (), //Todo: Do nothing because bevy's app init state isn't reflected in winit, so we need to early bail until init has completed
            (_, None, _) => panic!("WINDOW RES NOT FOUND FOR IMGUI EVENT FORWARDER"),
            (_, _, None) => {
                panic!("IMGUI Context not found for imgui event forwarder, should never happen")
            }
        };

        match main_event {
            Event::WindowEvent { ref event, .. } => {
                match event {
                    WindowEvent::KeyboardInput { input, .. } => {
                        //see if imgui wants to forward keyboard or not
                        if should_send_key {
                            info!("Sending keyboard to bevy");
                            let mut keyboard_input_events =
                                world.get_resource_mut::<Events<KeyboardInput>>().unwrap();
                            keyboard_input_events.send(convert_keyboard_input(input));
                        }
                    }

                    WindowEvent::MouseInput { state, button, .. } => {
                        if should_send_mouse {
                            let mut mouse_input_events = world
                                .get_resource_mut::<Events<MouseButtonInput>>()
                                .unwrap();
                            mouse_input_events.send(MouseButtonInput {
                                button: convert_mouse_button(*button),
                                state: convert_element_state(*state),
                            })
                        }
                    }

                    WindowEvent::CursorMoved { position, .. } => {
                        // Alaways send cursor position events, imgui doesn't need to consume these.
                        let mut mouse_delta_events =
                            world.get_resource_mut::<Events<MouseMotion>>().unwrap();
                        let mut mouse_pos_events = world
                            .get_resource_mut::<Events<CursorPositionChangedEvent>>()
                            .unwrap();
                        let concrete_position = convert_mouse_position(position);
                        let delta = concrete_position - previous_cursor_pos;
                        mouse_delta_events.send(MouseMotion { delta });
                        mouse_pos_events.send(CursorPositionChangedEvent {
                            new_position: convert_mouse_position(position),
                        });
                        previous_cursor_pos = concrete_position;
                    }

                    WindowEvent::Resized(physical_size) => {
                        if physical_size.width > 0 && physical_size.height > 0 {
                            trace!(
                                "Resize requested to {}x{}",
                                &physical_size.width,
                                &physical_size.height
                            );

                            let mut resize_events = world
                                .get_resource_mut::<Events<WindowResizedEvent>>()
                                .unwrap();

                            resize_events.send(WindowResizedEvent {
                                new_size: *physical_size,
                            });
                            active = true;
                        } else {
                            //Minimized window, so we need to stop rendering as we no longer have a valid surface
                            active = false;
                        }
                    }

                    WindowEvent::CloseRequested => {
                        trace!("Close requested event received");
                        *control_flow = ControlFlow::Exit;
                    }

                    _ => (),
                }
            }
            Event::RedrawRequested(_) => {
                trace!("Redraw requested");
            }
            Event::LoopDestroyed => {
                trace!("Loop destroyed");
            }
            Event::MainEventsCleared => {
                trace!("Main events cleared, updating ecs");
                //Tick ECS system
                drop(world);
                if active {
                    // Deactivated means the window is minimized, TODO: Later only stop rendering at this point, not the whole ecs
                    app.update();
                }

                trace!("Finished updating ECS");

                let new_time = std::time::Instant::now();
                let delta_time = new_time - old_time;
                *control_flow = if delta_time > MSPT {
                    ControlFlow::Poll
                } else {
                    trace!("Waiting until {:?}", old_time + MSPT);
                    ControlFlow::WaitUntil(old_time + MSPT)
                };

                old_time = new_time;
            }
            _ => (),
        }
    });
}
