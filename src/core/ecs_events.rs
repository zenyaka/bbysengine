use crate::renderer::imgui_systems::ImguiButtonType;
use bevy_math::Vec2;
use winit::dpi::PhysicalSize;

pub struct ImguiButtonEvent {
    pub button_clicked: ImguiButtonType,
}

pub struct WindowResizedEvent {
    pub new_size: PhysicalSize<u32>,
}

#[derive(Debug)]
pub struct CursorPositionChangedEvent {
    pub new_position: Vec2,
}
