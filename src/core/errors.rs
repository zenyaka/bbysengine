use crate::bevy_engine_layer::In;
use anyhow::Result;
use log::error;

pub fn system_error_handler(In(result): In<Result<()>>) {
    if let Err(err) = result {
        error!("System execution encountered error: {:?}", err);
    }
}
