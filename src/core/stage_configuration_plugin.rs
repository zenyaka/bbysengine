use crate::bevy_engine_layer::*;
use crate::GameState;

#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
pub enum StageExecutionLabel {
    RenderPrimary,
    RenderGui,
    PostRender,
}

// Bevy-required impl for 'reasons'
impl StageLabel for StageExecutionLabel {
    fn dyn_clone(&self) -> Box<dyn StageLabel> {
        Box::new(*self)
    }
}

// piggyback on Bevy's internal traits for type-safe execution ordering gates used by add_system in various
// places to manually order specific internal engine core
#[allow(dead_code)]
#[derive(Copy, Clone, PartialEq, Eq, Debug, Hash)]
pub enum SystemExecutionLabel {
    MainRender,
    InputSystem,
    InitializeRender,
}

impl SystemLabel for SystemExecutionLabel {
    fn dyn_clone(&self) -> Box<dyn SystemLabel> {
        Box::new(*self)
    }
}

#[derive(Default)]
pub struct StageConfigurationPlugin;

impl Plugin for StageConfigurationPlugin {
    fn build(&self, app: &mut App) {
        //need to setup proper stages first, before other plugins attempt to use them
        app.add_stage_after(
            CoreStage::PostUpdate,
            StageExecutionLabel::RenderPrimary,
            SystemStage::parallel(),
        )
        .add_stage_after(
            StageExecutionLabel::RenderPrimary,
            StageExecutionLabel::RenderGui,
            SystemStage::parallel(),
        )
        .add_stage_after(
            StageExecutionLabel::RenderGui,
            StageExecutionLabel::PostRender,
            SystemStage::parallel(),
        )
        .add_system_to_stage(CoreStage::PreUpdate, update_frame_time);
    }
}

pub fn update_frame_time(mut game_state: ResMut<GameState>) {
    game_state.dt.update();
}
