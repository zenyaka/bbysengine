use log::info;
use std::fmt::Debug;
use std::sync::Arc;

use imgui::{ColorButton, Condition, Context, FontSource};
use imgui_wgpu::{Renderer, RendererConfig};
use imgui_winit_support::WinitPlatform;
use wgpu::{Device, Queue};

use crate::bevy_engine_layer::World;
use crate::bevy_engine_layer::*;
use crate::core::ecs_events::ImguiButtonEvent;
use crate::core::errors::system_error_handler;
use crate::core::stage_configuration_plugin::{StageExecutionLabel, SystemExecutionLabel};
use crate::core::window_plugin::Window;
use crate::renderer::render_systems::SurfaceFrameCarrier;
use crate::renderer::RenderState;
use anyhow::Result;

use crate::GameState;

fn imgui_button_event_listener_system(mut my_event_reader: EventReader<ImguiButtonEvent>) {
    for _my_event in my_event_reader.iter() {
        //This is only run when one of those events is dispatched via events.send, as above in rendering system
        info!("Received IMGUI button click event successfully");
    }
}

#[derive(Default)]
pub struct ImguiPlugin;

impl Plugin for ImguiPlugin {
    fn build(&self, app: &mut App) {
        app.add_event::<ImguiButtonEvent>()
            .insert_resource(ImguiGameState {
                test_string: "Test String".to_string(),
            })
            .add_system(chain_test_system.chain(system_error_handler))
            .add_startup_system(
                initialize_imgui_renderer_sys
                    .exclusive_system()
                    .after(SystemExecutionLabel::InitializeRender),
            )
            .add_system_to_stage(StageExecutionLabel::RenderGui, imgui_render_sys)
            .add_system(imgui_button_event_listener_system)
            .add_system_to_stage(CoreStage::PreUpdate, update_imgui_frame);
    }
}

pub struct ImguiGameState {
    pub test_string: String,
}

pub fn imgui_render_sys(
    render_state: ResMut<RenderState>,
    mut frame_carrier: ResMut<SurfaceFrameCarrier>,
    window: ResMut<Window>,
    mut imgui_ctx: NonSendMut<ImguiCtxState>,
    imgui_platform: NonSendMut<ImguiPlatformState>,
    mut imgui_renderer: NonSendMut<ImguiRendererState>,
    mut events: EventWriter<ImguiButtonEvent>,
    mut imgui_game_state: ResMut<ImguiGameState>,
) {
    // We need to fish out the frame previously acquired from render_sys and render on top of it rather than acquiring a new one
    let frame = frame_carrier
        .frame
        .take()
        .expect("Surface primary texture not found in imgui_render_sys");

    imgui_platform
        .platform
        .prepare_frame(imgui_ctx.context.io_mut(), &window.window)
        .expect("Failed to prepare frame");
    let ui = imgui_ctx.context.frame();

    {
        let im_window = imgui::Window::new("Hello world");
        im_window
            .size([300.0, 300.0], Condition::FirstUseEver)
            .build(&ui, || {
                ui.text("Hello world");
                ui.separator();
                let mouse_pos = ui.io().mouse_pos;
                ui.text(format!(
                    "Mouse Position: ({:.1},{:.1})",
                    mouse_pos[0], mouse_pos[1]
                ));

                ui.text("This is a button:");
                if ColorButton::new("Green color", [0.0, 1.0, 0.0, 1.0])
                    .size([100.0, 50.0])
                    .tooltip(true)
                    .build(&ui)
                {
                    let imguibtn = ImguiButtonEvent {
                        button_clicked: ImguiButtonType::GreenBastard(
                            "Test event msg for green bastard".to_owned(),
                        ),
                    };

                    events.send(imguibtn);
                }
                ui.separator();
                ui.input_text("Test text input label", &mut imgui_game_state.test_string)
                    .build();
            });
    }

    let mut encoder: wgpu::CommandEncoder =
        render_state
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder2"),
            });

    //Todo: We should create this view once and pass it around instead of the surface format itself
    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
        label: Some("Render Pass"),
        color_attachments: &[wgpu::RenderPassColorAttachment {
            view: &view,
            resolve_target: None,
            ops: wgpu::Operations {
                // DO NOT CLEAR THE SCREEN HERE, imgui needs to be drawn on top
                load: wgpu::LoadOp::Load,
                store: true,
            },
        }],
        depth_stencil_attachment: None,
    });

    imgui_renderer
        .renderer
        .render(
            ui.render(),
            &render_state.queue,
            &render_state.device,
            &mut rpass,
        )
        .expect("Rendering failed");

    drop(rpass);

    render_state.queue.submit(std::iter::once(encoder.finish()));

    frame_carrier.frame = Some(frame);
}

pub fn update_imgui_frame(game_state: Res<GameState>, mut imgui: NonSendMut<ImguiCtxState>) {
    //This will explode almost immediately if removed
    use cgmath::Zero;
    if game_state.dt.delta_seconds().is_zero() {
        return;
    }

    imgui
        .context
        .io_mut()
        .update_delta_time(game_state.dt.delta());
}

pub struct ImguiCtxState {
    pub context: imgui::Context,
}

pub struct ImguiPlatformState {
    pub platform: imgui_winit_support::WinitPlatform,
}

pub struct ImguiRendererState {
    pub renderer: imgui_wgpu::Renderer,
}

pub fn initialize_imgui_renderer_sys(world: &mut World) {
    let window = world.get_resource::<Window>().unwrap();
    let render_state = world.get_resource::<RenderState>().unwrap();

    let (context, platform, renderer) =
        create_imgui_renderer(&window, &render_state.queue, &render_state.device);

    world.insert_non_send(ImguiCtxState { context });
    world.insert_non_send(ImguiPlatformState { platform });
    world.insert_non_send(ImguiRendererState { renderer });
}

fn create_imgui_renderer(
    window: &Window,
    queue: &Queue,
    device: &Arc<Device>,
) -> (Context, WinitPlatform, imgui_wgpu::Renderer) {
    let mut imgui = imgui::Context::create();
    let mut platform = imgui_winit_support::WinitPlatform::init(&mut imgui);

    // TODO: i'm lazy rn, but stop doing this later
    let window = &window.window;

    platform.attach_window(
        imgui.io_mut(),
        window,
        imgui_winit_support::HiDpiMode::Default,
    );

    imgui.set_ini_filename(None);

    let hidpi_factor = window.scale_factor();
    let font_size = (13.0 * hidpi_factor) as f32;
    imgui.io_mut().font_global_scale = (1.0 / hidpi_factor) as f32;

    imgui.fonts().add_font(&[FontSource::DefaultFontData {
        config: Some(imgui::FontConfig {
            oversample_h: 1,
            pixel_snap_h: true,
            size_pixels: font_size,
            ..Default::default()
        }),
    }]);
    let size = window.inner_size();

    let surface_desc = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: wgpu::TextureFormat::Bgra8UnormSrgb,
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
    };

    let renderer_config = RendererConfig {
        texture_format: surface_desc.format,
        ..Default::default()
    };
    let renderer = Renderer::new(&mut imgui, &device, &queue, renderer_config);

    (imgui, platform, renderer)
}

#[derive(Debug)]
pub enum ImguiButtonType {
    GreenBastard(String),
    //PartsUnknown(String),
}

fn chain_test_system() -> Result<()> {
    Ok(())
    //Err(anyhow::anyhow!("Testing error handler"))
}
