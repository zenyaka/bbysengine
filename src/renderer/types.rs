use bytemuck::Pod;

pub mod pipelines {
    use std::collections::HashMap;
    use wgpu::BindGroupLayout;

    // Pipeline wraps the core wgpu pipeline and associated bind group layout bc layouts
    // can be associated with multiple bind groups.  For now actual bind groups are held
    // inside of the Texture component, as we have no shared/batched bind groups
    #[derive(Debug)]
    pub struct Pipeline {
        pub pipeline: wgpu::RenderPipeline,
        pub bind_group_layout: Option<BindGroupLayout>,
    }

    pub type PipelineCache = HashMap<PipelineType, Pipeline>;

    #[derive(Eq, PartialEq, Hash, Debug)]
    pub enum PipelineType {
        Basic,
        Textured,
        ImGui,
    }
}

pub mod buffers {
    use crate::renderer::types::Vertex;

    pub struct VertexBuffer(pub wgpu::Buffer);
    pub struct IndexBuffer(pub wgpu::Buffer);
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
pub struct Vertex {
    pub position: [f32; 3],
    pub tex_coords: [f32; 2],
}

impl Vertex {
    pub fn desc<'a>() -> wgpu::VertexBufferLayout<'a> {
        wgpu::VertexBufferLayout {
            array_stride: std::mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute { offset: 0, shader_location: 0, format: wgpu::VertexFormat::Float32x3 },
                wgpu::VertexAttribute {
                    offset: std::mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x2,
                },
            ],
        }
    }
}
