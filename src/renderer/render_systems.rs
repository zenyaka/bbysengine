use futures_lite::future;
use log::trace;
use std::ops::Deref;
use std::sync::Arc;

use wgpu::{
    Adapter, Device, Instance, Queue, RenderPipeline, ShaderModule, Surface, SurfaceTexture,
    TextureFormat,
};
use winit::dpi;

use crate::bevy_engine_layer::*;
use crate::core::ecs_events::WindowResizedEvent;
use crate::core::stage_configuration_plugin::{StageExecutionLabel, SystemExecutionLabel};
use crate::core::window_plugin::Window;
use crate::renderer::types::buffers::{IndexBuffer, VertexBuffer};
use crate::renderer::types::pipelines::{Pipeline, PipelineCache, PipelineType};
use crate::renderer::types::Vertex;
use std::borrow::Cow;
use std::collections::HashMap;

#[derive(Default)]
pub struct RenderPlugin;

impl Plugin for RenderPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system_to_stage(
            StartupStage::Startup,
            initialize_renderer_sys
                .exclusive_system()
                .label(SystemExecutionLabel::InitializeRender),
        )
        .add_startup_system_to_stage(StartupStage::Startup, init_test_sys)
        .add_system_to_stage(
            StageExecutionLabel::RenderPrimary,
            render_sys.label(SystemExecutionLabel::MainRender),
        )
        .add_system_to_stage(StageExecutionLabel::PostRender, post_render_sys)
        .add_system_to_stage(CoreStage::PostUpdate, resize_window_event_handler)
        .add_event::<WindowResizedEvent>();
    }
}

pub fn post_render_sys(mut surface_frame: ResMut<SurfaceFrameCarrier>) {
    trace!("Dropping surface frame to force present");

    let final_frame = surface_frame
        .frame
        .take()
        .expect("Frame missing from carrier in post-render");
    final_frame.present();
}

pub fn init_test_sys() {
    log::info!("Initted");
}

#[derive(Debug)]
pub struct RenderState {
    pub surface: wgpu::Surface,
    pub device: Arc<wgpu::Device>,
    pub queue: wgpu::Queue,
    pub size: dpi::PhysicalSize<u32>,
    pub render_pipeline_cache: PipelineCache,
    pub surface_format: TextureFormat,
    pub surface_desc: wgpu::SurfaceConfiguration,
}

pub fn resize_window_event_handler(
    mut resize_events: EventReader<WindowResizedEvent>,
    mut render_state: ResMut<RenderState>,
) {
    //TODO: Make this wait until drag is over before executing so we don't recreate the surface every single frame by 1 pixel each dir
    for event in resize_events.iter() {
        render_state.surface_desc.width = event.new_size.width;
        render_state.surface_desc.height = event.new_size.height;

        //TODO: Abstract this into own fn
        render_state
            .surface
            .configure(&*render_state.device, &render_state.surface_desc);
    }
}

//Base rendering system which fires every frame. All it does is prep the frame, then clear the screen
pub fn render_sys(
    render_state: ResMut<RenderState>,
    mut surface_frame: ResMut<SurfaceFrameCarrier>,
) {
    let frame = render_state
        .surface
        .get_current_texture()
        .expect("Failed to acquire next swap chain texture");

    let view = frame
        .texture
        .create_view(&wgpu::TextureViewDescriptor::default());

    let mut encoder = render_state
        .device
        .create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Render Encoder"),
        });

    //clear the screen
    encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
        label: Some("Render Pass"),
        color_attachments: &[wgpu::RenderPassColorAttachment {
            view: &view,
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Clear(wgpu::Color {
                    r: 0.1,
                    g: 0.2,
                    b: 0.3,
                    a: 1.0,
                }),
                store: true,
            },
        }],
        depth_stencil_attachment: None,
    });

    // Render main
    render_state.queue.submit(std::iter::once(encoder.finish()));

    //store the acquired frame for further rendering (imgui)
    surface_frame.frame = Some(frame)
}

// WGPU's surface will spit out a texture frame exactly once per 'rendered' frame and
// output that texture to the screen when it goes out of scope.  We need to render
// at least two passes (scene + imgui) so we need to store it between them as an ecs resource
pub struct SurfaceFrameCarrier {
    pub frame: Option<SurfaceTexture>,
}

pub fn initialize_renderer_sys(world: &mut World) {
    let window = world.get_resource_mut::<Window>().unwrap();
    let render_state = future::block_on(create_renderers(window));

    world.insert_resource(render_state);
    world.insert_resource(SurfaceFrameCarrier { frame: None });
}

async fn create_renderers(mut window: Mut<'_, Window>) -> RenderState {
    let winit_window = &mut window.window;
    let size = winit_window.inner_size();

    let instance = wgpu::Instance::new(wgpu::Backends::PRIMARY);
    let surface = unsafe { instance.create_surface(winit_window.deref()) };

    let (queue, device, adapter) = create_main_device(instance, &surface).await;
    let shader = compile_shaders(&device);

    //create render pipeline layout
    let surface_format = surface
        .get_preferred_format(&adapter)
        .expect("Couldn't get preferred swapchain format from adapter");

    // Need to shuttle surface_format back and forth bc yay rust ownership.  We need to persist it for window resize swapchain later
    let (render_pipeline, surface_format) =
        create_render_pipeline(&device, &shader, surface_format);

    //TODO: Abstract this to own fn so it can be called from window resize handler too
    let surface_desc = wgpu::SurfaceConfiguration {
        usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
        format: surface_format,
        width: size.width,
        height: size.height,
        present_mode: wgpu::PresentMode::Mailbox,
    };

    //newish WGPU feature, lets you completely trade out a swapchain for new one just by passing a new config to surface
    surface.configure(&device, &surface_desc);

    let render_pipeline_cache = HashMap::from([(
        PipelineType::Basic,
        Pipeline {
            pipeline: render_pipeline,
            bind_group_layout: None,
        },
    )]);

    //let render_pipeline_cache = PipelineCache::from([
    //    (PipelineType::Basic, render_pipeline),
    //]);

    //Done, package everything up and return a renderstate we can fish out of the ECS when needed
    let render_state_resource = RenderState {
        surface,
        device,
        queue,
        size,
        render_pipeline_cache,
        surface_format,
        surface_desc,
    };

    render_state_resource
}

fn create_render_pipeline(
    device: &Arc<Device>,
    shader: &ShaderModule,
    swapchain_format: TextureFormat,
) -> (RenderPipeline, TextureFormat) {
    let render_pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
        label: Some("Render Pipeline Layout"),
        bind_group_layouts: &[],
        push_constant_ranges: &[],
    });

    let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(&render_pipeline_layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main", // shader main fn to call, can just always be main
            buffers: &[Vertex::desc()], // type of vertices to pass to vertex shader
        },
        fragment: Some(wgpu::FragmentState {
            // This is optional, but required to store color data to swap chain
            module: &shader,
            entry_point: "fs_main",
            targets: &[swapchain_format.into()],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::TriangleList,
            strip_index_format: None,
            front_face: wgpu::FrontFace::Ccw, // 5
            cull_mode: Some(wgpu::Face::Back),
            // Requires Features::DEPTH_CLAMPING
            clamp_depth: false,
            // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
            polygon_mode: wgpu::PolygonMode::Fill,
            // Requires Features::CONSERVATIVE_RASTERIZATION
            conservative: false,
        },
        depth_stencil: None, // Todo: Set up depth stencil
        multisample: wgpu::MultisampleState {
            count: 1,                         // just disable it for now
            mask: !0,                         // Use every sample
            alpha_to_coverage_enabled: false, // This is for AA l8r
        },
    });
    (render_pipeline, swapchain_format)
}

fn compile_shaders(device: &Arc<Device>) -> ShaderModule {
    let shader_src =
        std::fs::read_to_string("assets/shaders/shader.wsgl").expect("Couldn't read shader.wsgl");
    let shader = device.create_shader_module(&wgpu::ShaderModuleDescriptor {
        label: Some("BasicShader"),
        source: wgpu::ShaderSource::Wgsl(Cow::from(shader_src)).into(),
    });

    shader
}

async fn create_main_device(
    instance: Instance,
    surface: &Surface,
) -> (Queue, Arc<Device>, Adapter) {
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        })
        .await
        .expect("Unable to find a GPU! Make sure you have installed required drivers!");

    //Uncomment to turn on debug tracing - warning it is quite a lot
    //let trace_path = Some(Path::new("C:/trace"));
    let trace_path = None;

    let (device, queue) = adapter
        .request_device(
            &wgpu::DeviceDescriptor {
                label: None,
                features: wgpu::Features::empty(),
                limits: wgpu::Limits::default(),
            },
            trace_path,
        )
        .await
        .unwrap();
    let device = Arc::new(device);
    (queue, device, adapter)
}
