use crate::renderer::types::buffers::{IndexBuffer, VertexBuffer};

pub struct RenderComp {
    pub vertex_buffer: VertexBuffer,
    pub num_vertices: u32,
    pub index_buffer: Option<IndexBuffer>,
    pub num_indices: u32,
}
