mod bevy_engine_layer;
mod core;
mod demos;
mod renderer;

#[allow(unused_imports)]
use log::{debug, error, info, log_enabled, trace, Level};

use crate::bevy_engine_layer::{App, BevyInternalPlugins, Time, Timer};
use crate::core::input_systems::InputHandlingPlugin;
use crate::core::stage_configuration_plugin::StageConfigurationPlugin;
use crate::core::window_plugin::WindowPlugin;
use crate::demos::boids_compute::BoidsComputePlugin;
use crate::renderer::imgui_systems::ImguiPlugin;
use crate::renderer::render_systems::RenderPlugin;
use bevy_ecs::schedule::ReportExecutionOrderAmbiguities;

use crate::demos::textured::TextureRenderPlugin;
use wgpu::util::DeviceExt;

pub struct DebugPrintTimer {
    pub global: Timer,
}

#[derive(Debug)]
pub struct GameState {
    pub game_name: String,
    pub game_version: String,
    pub dt: Time,
}

impl Default for GameState {
    fn default() -> Self {
        GameState {
            game_name: "ZenGame".to_string(),
            game_version: "0.0.3".to_string(),
            dt: Default::default(),
        }
    }
}

fn main() {
    init_logging();

    App::new()
        .insert_resource(ReportExecutionOrderAmbiguities)
        //This adds Time and Timer systems and resources
        .add_plugins(BevyInternalPlugins)
        .add_plugin(WindowPlugin)
        .init_resource::<GameState>()
        .insert_resource(DebugPrintTimer {
            global: Timer::from_seconds(1.0, true),
        }) //example
        .add_plugin(StageConfigurationPlugin) //This has to happen after GameState init
        .add_plugin(RenderPlugin)
        .add_plugin(ImguiPlugin)
        .add_plugin(TextureRenderPlugin)
        .add_plugin(InputHandlingPlugin)
        //.add_plugin(BoidsComputePlugin)
        .run();
}

fn init_logging() {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "{}<{}>[{}] {}",
                chrono::Local::now().format("[%H:%M:%S]"),
                record.target(),
                record.level(),
                message
            ))
        })
        // Set warn by default because wgpu is ultra chatty
        .level(log::LevelFilter::Warn)
        .level_for("bbysengine", log::LevelFilter::Info)
        .chain(std::io::stdout())
        //.chain(fern::log_file("output.log").unwrap())
        .apply()
        .unwrap();
}
