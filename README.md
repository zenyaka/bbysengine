To update this with future versions of Bevy:

NO changes need to be made, just replace all of the
/crates crates directly with the ones from Bevy.  That's all.

Any new functionality can be exposed in bevy_engine_layer.
